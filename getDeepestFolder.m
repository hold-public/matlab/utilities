function deepestFolder = getDeepestFolder(path)
    pathElements = strsplit(path, filesep);
    deepestFolder = pathElements{end};
end