function cellArray = insertToCellArray(cellArray, insertCellArray, row, column)
    insertedRows = size(insertCellArray, 1);
    insertedColumns = size(insertCellArray, 2);
    cellArray(row:row + insertedRows - 1, column:column + insertedColumns - 1) = insertCellArray;
end
