function sec = timeStrToSec(timeStr)
    colonPos = strfind(timeStr, ':');
    hours = str2double(timeStr(1:colonPos(1) - 1));
    if length(colonPos) == 1 
        mins = str2double(timeStr(colonPos + 1:end));
        secs = 0;
    elseif length(colonPos) == 2
        mins = str2double(timeStr(colonPos(1) + 1:colonPos(2) - 1));
        secs = str2double(timeStr(colonPos(2) + 1:end));
    else
        warning('time not recognized!')
        secs = -1;
    end
    sec = 3600 * hours + 60 * mins + secs;
end