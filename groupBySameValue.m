function [groupedObjects, groupValues] = groupBySameValue(objects, values)
    nrOfObjects = length(objects);
    if nrOfObjects ~= length(values)
        warning('to group objects, the same number of objects and values must be delivered, fucker!')
    end
    groupValues = unique(values, 'stable');
    nrOfUniqueValues = length(groupValues);
    groupedObjects = cell(nrOfUniqueValues, 1);
    for currentObjectIndex = 1:nrOfObjects
        currentObject = objects(currentObjectIndex);
        groupIndex = values(currentObjectIndex) == groupValues;
        groupedObjects{groupIndex} = [groupedObjects{groupIndex}; currentObject];
    end
end