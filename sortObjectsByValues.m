function sortedObjects = sortObjectsByValues(objects, values)
    [~, sortingIndexes] = sort(values);
    sortedObjects = objects(sortingIndexes);
end