function newVector = insertVal(originalVector, value, pos)
    if size(originalVector, 2) > 1 % if row vector
        newVector = toColumnVector(originalVector);
    else
        newVector = originalVector;
    end
    newVector = [newVector(1:pos-1); value; newVector(pos:end)];
    if size(originalVector, 2) > 1 % if row vector
        newVector = toRowVector(newVector);
    end
end