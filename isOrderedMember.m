function isOrderedMember = isOrderedMember(searchSet, superSet)
    nrOfSearchEntries = length(searchSet);
    superSetSearchBeginning = 1;
    isOrderedMember = true;
    for searchIndex = 1:nrOfSearchEntries
        foundPos = find(searchSet(searchIndex) == superSet(superSetSearchBeginning:end), 1, 'first');
        if ~isempty(foundPos)
            superSetSearchBeginning = superSetSearchBeginning + foundPos;
        else
            isOrderedMember = false;
        end
    end
end