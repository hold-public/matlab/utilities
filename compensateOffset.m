function yComp = compensateOffset(yRaw, minNrOfSameValues, maxOffset, maxChange)
	% find some values that have the same value, and that are close to 0
    valueChanges = [true; abs(diff(yRaw)) > abs(maxChange)];
    newValuePos = find(valueChanges);
    nrOfSameValues = diff([newValuePos; length(yRaw) + 1]);
    enoughSameValues = nrOfSameValues >= minNrOfSameValues;
    candidateValues = yRaw(newValuePos);
    smallerThanMaxOffset = abs(candidateValues) < abs(maxOffset);
    isPossibleOffset = enoughSameValues & smallerThanMaxOffset;
    if any(isPossibleOffset)
        [~, longestOffsetIndex] = max(nrOfSameValues .* isPossibleOffset);
        offset = mean(yRaw(newValuePos(longestOffsetIndex):newValuePos(longestOffsetIndex) + nrOfSameValues(longestOffsetIndex) - 1));
    else
        offset = 0;
    end
    % compensate values by found offset value
    yComp = yRaw - offset;
end