function symmetryRadius = getSymmetryRadius(data, symmetryCenterIndex)
    totalLength = length(data);
    symmetryRadius = 0;
    allConditionsOk = true;
    while allConditionsOk
        spaceOnLowerSide = symmetryCenterIndex > symmetryRadius;
        spaceOnUpperSide = symmetryCenterIndex < totalLength - symmetryRadius + 1;
        if spaceOnLowerSide && spaceOnUpperSide
            allConditionsOk = data(symmetryCenterIndex - symmetryRadius) == data(symmetryCenterIndex + symmetryRadius);
            if allConditionsOk
            	symmetryRadius = symmetryRadius + 1;
            end
        else
            allConditionsOk = false;
        end
    end
    symmetryRadius = symmetryRadius - 1;
end