function goodColors = getGoodColors(m)
    allColors = [0   0   255;... %dark blue
                 0   255 255;... %turquise
                 255 128 0  ;... %dark orange
                 255 0   0  ;... %red
                 217 0   217;... %dark purple
                 0   128 0  ;... %dark green
                 0   244 0  ] / 255; %middle green
    nrOfAvailableColors = size(allColors, 1);
    if m <= nrOfAvailableColors
    	goodColors = allColors(1:m, :);
    else
    	warning('not enough colors!')
    	goodColors = repmat(allColors, ceil(m / size(allColors, 1)), 1);
        goodColors = goodColors(1:m, :);
    end
end