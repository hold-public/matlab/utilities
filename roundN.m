function roundedNumber = roundN(number, digitsAfterComma)
    if rem(digitsAfterComma, 1) %if whole number of digits after comma
        warning('cannot round to non-integer number!')
    end
	multiFactor = 10^digitsAfterComma;
    
    roundedNumber = 1/multiFactor * round(number * multiFactor);
end