function vector = toColumnVector(vector)
    if size(vector, 2) > 1 && size(vector, 1) == 1
       vector = vector'; 
    end
end