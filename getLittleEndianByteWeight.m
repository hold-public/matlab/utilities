function byteWeight = getLittleEndianByteWeight(lowerBitNr, higherBitNr)
    if higherBitNr >= lowerBitNr
        byteWeight = 256.^(lowerBitNr:higherBitNr)';
    else
        error('higherBitNr must be higher than or equal to lowerBitNr!')
    end
end
    