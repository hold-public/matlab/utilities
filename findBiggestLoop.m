function loopNrs = findBiggestLoop(values)
    valueDifferences = diff(values);
    symmetricalDifferences = valueDifferences(2:end) + valueDifferences(1:end-1) == 0;
    valueIsPeak = [false; symmetricalDifferences; false];
end