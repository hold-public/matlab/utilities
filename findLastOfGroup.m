function lastIndexes = findLastOfGroup(booleanValues)
    booleanValues = toColumnVector(booleanValues);
    reverseBooleanValues = flipud(booleanValues);
    complementaryLastIndexes = findFirstOfGroup(reverseBooleanValues);
    lastIndexes = flipud(length(booleanValues) + 1 - complementaryLastIndexes);
end