function flipped = flip(values)
    dimension = size(values);
    distinctDimension = find(dimension > 1);
    if length(distinctDimension) < 2
        flipped = flipdim(values, distinctDimension);
    else
        warning('cannot flip this object! Too many distinct dimensions')
        flipped = values;
    end
end