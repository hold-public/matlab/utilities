function [groupedObjects, groupValues] = groupBySameAdjacentValue(objects, values)
    objects = toColumnVector(objects);
    values = toColumnVector(values);
    
    nrOfObjects = length(objects);
    if nrOfObjects ~= length(values)
        warning('to group objects, the same number of objects and values must be delivered, fucker!')
    end
    isNewValue = [true; diff(values) ~= 0];
    newValueIndex = find(isNewValue);
    endOfNewValueIndex = [newValueIndex(2:end) - 1; nrOfObjects];
    nrOfGroups = length(newValueIndex);
    groupedObjects = cell(nrOfGroups, 1);
    groupValues = zeros(nrOfGroups, 1);
    for currentGroupNr = 1:nrOfGroups
        startIndex = newValueIndex(currentGroupNr);
        endIndex = endOfNewValueIndex(currentGroupNr);
        groupedObjects{currentGroupNr, 1} = objects(startIndex:endIndex); 
        groupValues(currentGroupNr, 1) = values(startIndex);
    end
end