function contourfWithParams(plotData, plotFormatting)
    plot_title = plotData.plot_title;
    xData = plotData.xData;
    yData = plotData.yData;
    zData = plotData.zData;
    xLim = plotFormatting.xLim;
    yLim = plotFormatting.yLim;
    xScale = plotFormatting.xScale;
    yScale = plotFormatting.yScale;
    xLabel = plotFormatting.xLabel;
    yLabel = plotFormatting.yLabel;
    xTick = plotFormatting.xTick;
    yTick = plotFormatting.yTick;
    xDir = plotFormatting.xDir;
    yDir = plotFormatting.yDir;
    cmap = plotFormatting.cmap;
    resolution = plotFormatting.resolution;
    labelSpacing = plotFormatting.labelSpacing;

    nrOfDataPoints = nnz(~isnan(zData));
    if nrOfDataPoints < 6
        error(['contour function needs at least 6 points to be able to plot!. '...
            'Graph ''' plot_title ''' has only ' nrOfDataPoints ' points. Please add some!']);
    end
    
    % maxima values
    minXValue = min(xData);
    maxXValue = max(xData);
    minYValue = min(yData);
    maxYValue = max(yData);

    % prepare range
    rangeX = linspace(minXValue, maxXValue, resolution);
    rangeY = linspace(minYValue, maxYValue, resolution);
    [X, Y] = meshgrid(rangeX,rangeY); 

    polydegreeX = min(2, length(xData) - 1);
    polydegreeY = min(2, length(yData) - 1);
    ft = fittype(['poly' num2str(polydegreeX) num2str(polydegreeY)]);

    [fitFunction, ~] = createFit(xData, yData, zData, ft);

    zFitted = feval(fitFunction, X, Y);

    %levels=[80:1:98];
    if size(zData, 1) >= 2 && size(zData, 2) >= 2
        figure
        [C,h] = contourf(rangeX * xScale, rangeY * yScale, zFitted);
        title(plot_title)
        clabel(C, h, 'LabelSpacing', labelSpacing)
        xlabel(xLabel)
        ylabel(yLabel)
        xlim(xLim * xScale)
        ylim(yLim * yScale)
        if ~strcmp(xTick, 'auto') %~strcmp(xLim, 'auto') &&
            if ~isscalar(xTick)
                set(gca,'XTick', xTick);
            else
                currentLimits = get(gca,'XLim'); 
                set(gca,'XTick', currentLimits(1):xTick:currentLimits(2));
            end
        end
        if ~strcmp(yTick, 'auto')
            if ~isscalar(yTick)
                set(gca,'YTick', yTick);
            else
                currentLimits = get(gca,'YLim'); 
                set(gca,'YTick', currentLimits(1):yTick:currentLimits(2));
            end
        end
        set(gca,'XDir', xDir)
        set(gca,'YDir', yDir)
        colormap(cmap)
    else 
    	warning([plot_title ' omitted - not enough data'])
    end
end