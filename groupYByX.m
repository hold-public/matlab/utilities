function [xGroups, yMeans] = groupYByX(X, Y, nGroups)
    [nElements, xGroups] = hist(X, nGroups);
    endIndexes = cumsum(nElements');
    sortedData = sortrows([X, Y], 1);
    
    yMeans = zeros(nGroups, 1);
    for groupNr = 1:nGroups
        if nElements(groupNr) > 0
            startIndex = endIndexes(groupNr) - nElements(groupNr) + 1;
            endIndex = endIndexes(groupNr);
            relevantIndexes = startIndex: endIndex;
            yMeans(groupNr) = mean(sortedData(relevantIndexes, 2));
        else
            yMeans(groupNr) = 0;
        end
    end
end