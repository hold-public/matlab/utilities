function hourOfDay = secToHourOfDay(sec)
    hourOfDay = ceil(sec / 3600);
end