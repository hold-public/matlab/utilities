function deleteOutputFilesInFolder(folderPath)
    fullFilePath = [folderPath '\*.xlsx'];
    filesToDelete = dir(fullFilePath);
    if ~isempty(filesToDelete)
        deletePersistent(fullFilePath) 
    end
    
    fullFilePath = [folderPath '\*.png'];
    filesToDelete = dir(fullFilePath);
    if ~isempty(filesToDelete)
        deletePersistent(fullFilePath) 
    end
    
    fullFilePath = [folderPath '\*.fig'];
    filesToDelete = dir(fullFilePath);
    if ~isempty(filesToDelete)
        deletePersistent(fullFilePath) 
    end
    
    fullFilePath = [folderPath '\*.kml'];
    filesToDelete = dir(fullFilePath);
    if ~isempty(filesToDelete)
        deletePersistent(fullFilePath)
    end
end