function timeStr = secToTimeStr(sec)
    days = sec / (24 * 3600);
    if days >= 1
        if days >= 2
            preString = [num2str(floor(days)) ' days, '];
        else
            preString = [num2str(floor(days)) ' day, '];
        end
    else
        preString = '';
    end
    timeStr = [preString datestr(days, 'HH:MM:SS')];
end
