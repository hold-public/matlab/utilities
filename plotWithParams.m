function plotWithParams(plotData, plotFormatting)
    plot_title = plotData.plot_title;
    xData = plotData.xData;
    yData = plotData.yData;
    xLim = plotFormatting.xLim;
    yLim = plotFormatting.yLim;
    xScale = plotFormatting.xScale;
    yScale = plotFormatting.yScale;
    xLabel = plotFormatting.xLabel;
    yLabel = plotFormatting.yLabel;
    xTick = plotFormatting.xTick;
    yTick = plotFormatting.yTick;
    xDir = plotFormatting.xDir;
    yDir = plotFormatting.yDir;
    cmap = plotFormatting.cmap;
    legendTexts = plotFormatting.legendTexts;

    figure('NumberTitle', 'Off','Name', plot_title, 'Position', Consts.SCREEN_SIZE)
    hold on
    
    nrOfTests= length(xData);
    h_vect = zeros(nrOfTests, 1);
    for i=1:nrOfTests
        h = plot(xData{i} * xScale, yData{i} * yScale, '-', 'Color', cmap(i,:));
        h_vect(i) = h;
    end

    set(gca,'FontSize',Consts.GRAPH_FONT_SIZE);
    set(get(gca,'Children'),'LineWidth',Consts.GRAPH_LINEWIDTH)
    legend(h_vect,legendTexts,'Location','EastOutside')
    set(gca,'Position',Consts.AXIS_POSITION)

    title(plot_title)
    xlabel(xLabel)
    ylabel(yLabel)
    xlim(xLim)
    ylim(yLim)
    if ~strcmp(xTick, 'auto') 
        if ~isscalar(xTick)
            set(gca,'XTick', xTick);
        else
            currentLimits = get(gca,'XLim'); 
            set(gca,'XTick', currentLimits(1):xTick:currentLimits(2));
        end
    end
    if ~strcmp(yTick, 'auto')
        if ~isscalar(yTick)
            set(gca,'YTick', yTick);
        else
            currentLimits = get(gca,'YLim'); 
            set(gca,'YTick', currentLimits(1):yTick:currentLimits(2));
        end
    end
    set(gca,'XDir', xDir)
    set(gca,'YDir', yDir)
    grid
end