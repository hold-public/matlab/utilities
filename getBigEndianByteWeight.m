function byteWeight = getBigEndianByteWeight(lowerBitNr, higherBitNr)
    if higherBitNr >= lowerBitNr
        byteWeight = flipud(256.^(lowerBitNr:higherBitNr)');
    else
        error('higherBitNr must be higher than or equal to lowerBitNr!')
    end
end
    