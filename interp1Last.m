function ys = interp1Last(x, y, xs)
    [~, bin] = histc(xs, x);
    ys = y(bin);
end