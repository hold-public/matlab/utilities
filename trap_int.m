function yInt = trap_int(x, y)
    dx = diff(x);
    yTrapez = (y(1:end-1) + y(2:end)) / 2;
    yInt = [0; cumsum(dx .* yTrapez)];
end