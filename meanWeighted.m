function weightedAverage = meanWeighted(values, weights)
   weightedAverage = sum(values .* weights) / sum(weights);
end