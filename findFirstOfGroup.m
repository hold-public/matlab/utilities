function firstIndexes = findFirstOfGroup(booleanValues)
    booleanValues = toColumnVector(booleanValues);
    if ~islogical(booleanValues)
       warning('no boolean value!') 
    end
    trueIndexes = find(booleanValues);
    if ~isempty(trueIndexes)
        distanceToLastTrueIndex = diff(trueIndexes);
        isFirstTrueIndex = [true; distanceToLastTrueIndex > 1];
        firstIndexes = trueIndexes(find(isFirstTrueIndex));
    else
    	firstIndexes = []; 
    end
end