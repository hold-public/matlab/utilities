function phaseOfDay = hourOfDayToPhaseOfDay(hourOfDay)
    phaseOfDay = ceil(hourOfDay / Consts.HOURS_PER_PHASE_OF_DAY);
end