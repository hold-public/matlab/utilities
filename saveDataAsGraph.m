function saveDataAsGraph(data, savePath, titleStr, fileExtension)
    figure(1)
    set(gcf,'Visible', 'off'); 
    plot(data(:, 1), data(:, 2), 'r');
 
    title(titleStr)
    xlabel('log time / s')
    ylabel(titleStr)
   
    fullFilePath = [savePath '\Graph ' titleStr '.' fileExtension];
    saveas(gcf, fullFilePath, fileExtension);
    close
end