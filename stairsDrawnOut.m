function stairsDrawnOut(arg1, varargin)
    if nargin > 1
        if isnumeric(varargin{1})
            warning('do not use this function with X values!');
        else
            Y = toColumnVector(arg1);
            stairs([Y; Y(end)], varargin{1:end});
        end
    else
        Y = toColumnVector(arg1);
        stairs([Y; Y(end)]);
    end
end