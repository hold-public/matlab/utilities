function removeSpareExcelSheets(filePath) 
% 
%xlserase: L�scht alle leeren Bl�tter aus einem Excel-File und formatiert 
%die Spaltenbreite auf "15" 
%xlserase(xfile) 
%xfile ist der Vollst�ndige Pfad- und Dateiname 


%�ffne Excel-File via ActiveX-Server 
xobject = actxserver('Excel.Application'); 
xworkbook = xobject.workbooks.Open(filePath); 
worksheets = xobject.sheets; 

%Anzahl der Vorhandenen Bl�tter 
nrOfSheets = worksheets.count; 


currentSheetNr = 1; 
for i = 1:nrOfSheets 
    
    %Formatierung der Spalten     
    %ber = get(worksheets.Item(h2),'Range','A1','IV1'); 
    %set(ber, 'ColumnWidth', 15); 
    
    %Hilfsvariable 
    h3 = worksheets.count; 
    
    %L�schen nicht m�glich, wenn nur 1 Blatt vorhanden 
    if h3 == 1 
        break 
    end 
    
    %L�scht Blatt, falls es leer ist 
    worksheets.Item(currentSheetNr).Delete; 
    
    %Falls Blatt nicht gel�scht, gehe zu n�chstem Blatt 
    if worksheets.count == h3
        currentSheetNr = currentSheetNr+1; 
    end 
end 

%Schlie�e Excel-File 
xworkbook.Save; 
xworkbook.Close(false); 
xobject.Quit; 
delete(xobject); 


%end f�r MF 
end 