function writeRawExcel(fullFilePath, sheet, headers, data)
    writingSuceeded = false;
    while(~writingSuceeded)
        try
            warning off MATLAB:xlswrite:AddSheet
            xlswrite(fullFilePath, headers, sheet, 'A1');
            xlswrite(fullFilePath, data, sheet, 'A2');
            writingSuceeded = true;
        catch err
            warning(['problem with file "' fullFilePath '". Maybe it is locked. Please close Excel sheet and try again.'])
            inputText = input('try again? y/n ', 's');
            if(~strcmp(inputText, 'y'))
                rethrow(err);
            end
        end
    end
end