function vector = toRowVector(vector)
    if size(vector, 1) > 1 && size(vector, 2) == 1
       vector = vector'; 
    end
end