function phaseOfDayString = getPhaseOfDayString(phaseNr)
	phaseOfDayString = sprintf('%02d.00 - %02d.59', (phaseNr - 1) * Consts.HOURS_PER_PHASE_OF_DAY, phaseNr * Consts.HOURS_PER_PHASE_OF_DAY - 1);
end